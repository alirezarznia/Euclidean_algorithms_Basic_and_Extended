#include 	<cstdio>
#include	<iostream>
#include	<cmath>
#include 	<cstring>
#include 	<cstdlib>
#include 	<vector>
#include 	<string>
#include 	<algorithm>
#include 	<queue>
#include 	<deque>
#include 	<set>
#include 	<stack>
#include 	<map>
#include 	<sstream>
#include 	<ctime>
#include	<iomanip>
#include 	<functional>

#define 	Time 		printf("\nTime : %.3lf s.\n", clock()*1.0/CLOCKS_PER_SEC)
#define 	For(J,R,K) 	for(ll J=R;J<K;++J)
#define 	Rep(I,N) 	For(I,0,N)
#define 	MP 			make_pair
#define 	ALL(X) 		(X).begin(),(X).end()
#define 	SF 			scanf
#define 	PF 			printf
#define 	pii 		pair<long long,long long>
#define 	pdd 		pair<double , double>
#define 	Sort(v) 	sort(ALL(v))
#define 	Test 		freopen("a.in","r",stdin)
#define 	Testout 	freopen("a.out","w",stdout)
#define 	pb 			push_back
#define 	Set(a,n) 	memset(a,n,sizeof(a))
#define 	MAXN 		100000+99
#define 	EPS 		1e-15
#define 	inf 		1ll<<60

typedef long long ll;
typedef long double ld;

using namespace std;
ll gcd;
ll GCD(ll a , ll b , ll &x , ll &y)
{
    if(b==0){
        y = 0 , x = 1;
        return a;
    }
    ll x1 , y1;
    gcd =GCD(b , (a%b) , x1 , y1);

    x= y1;
    y= x1- (a/b)*y1;
    return gcd;
}
int main()
{
 //   Test;
    ll t;
    cin>>t;
    while(t--)
    {
        ll x ,y, n , m;
        cin>> n >> m;
        cout<<GCD(n , m , x , y)<<endl;
        cout<<x<<" "<<y<<endl;
    }
}